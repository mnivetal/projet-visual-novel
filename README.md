# Projet Visual Novel 

## Table des contenus:
### Description
### Statut du projet
### Logiciels Libres utilisés
### Instruction d'installation
### Droit d'auteurs

## Description
Ce projet est un Visual Novel (jeu à choix multiples) dont les décisions prises au cours du jeu permettent d'aboutir à plusieurs fins toutes différentes.
Le jeu s'intitule Midnight Explorers et raconte l'histoire d'un groupe de 4 amis qui décident d'aller explorer de nuit un ancien hôpital abandonné.

## Statut du projet
Le projet a été terminé le 6 Mai 2021.

## Logiciels Libres utilisés
Nous avons utilisé le logiciel libre Renpy afin de réaliser l'entièreté de ce projet.
Celui ci a été conçu sous le langage de programmation Python.


## Instruction d'installation
Vous pourrez trouver dans le projet la version produite du jeu ne nécessitant aucune installation.

## Droit d'auteurs
Ce jeu a été créé grâce à la participation des collaborateurs Agathe Perrin, Romain Simon, Nicole Pearson, Estelle Ntsama.
Le jeu a été fait sur le logiciel libre **Renpy**.

